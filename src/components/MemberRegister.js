import axios from 'axios'
import React, { useState } from 'react'

export default function MenberRegister() {
    let [data1, setData1] = useState()
    const register = (data) => {
        axios.post(`http://localhost:3001/api/v1/users/register`, data).then((res) => {
            setData1(res.data.userInfo);
            console.log(res.data.userInfo);
        })
    }
    const [regData, setRegData] = useState({
        firstName: '',
        lastName: '',
        email: '',
        gender: '',
        phone: '',
        address: '',
        password: '',
        re_password: ''
    })
    const handleChange = (e) => {
        let controlledName = e.target.name;
        let controlledValue = e.target.value;
        setRegData({
            ...regData,
            [controlledName]: controlledValue
        })
    }
    const handleSubmit = (e) => {
        e.preventDefault()
        console.log(regData);
        register(regData)
    }
    return (
        <>
            <div className="container border col-sm-8 mx-auto m-5 p-3 bg-ligh shadow">
                <h4 className="my-2">Register Component</h4>
                <form onSubmit={handleSubmit}>
                    <div className="form-row">
                        <div className="form-group col-sm-6">
                            <label for="firstName">First Name</label>
                            <input
                                type="text"
                                id="firstName"
                                name="firstName"
                                onChange={handleChange}
                                class="form-control"
                                placeholder="Please Enter First Name" />
                        </div>
                        <div className="form-group col-sm-6">
                            <label for="lastName">Last Name</label>
                            <input
                                type="text"
                                id="lastName"
                                name="lastName"
                                onChange={handleChange}
                                class="form-control"
                                placeholder="Please Enter Last Name" />
                        </div>
                        <div className="form-group col-sm-6">
                            <label for="email">Email Address</label>
                            <input
                                type="text"
                                id="email"
                                name="email"
                                onChange={handleChange}
                                className="form-control"
                                placeholder="Enter Email Address" />
                        </div>
                        <div className="form-group col-sm-6">
                            <label for="gender">Gender</label>
                            <select className="form-control" id="gender" name="gender" onChange={handleChange}>
                                <option>Male</option>
                                <option>Female</option>
                                <option>Other</option>
                            </select>
                        </div>
                        <div className="form-group col-sm-6">
                            <label for="mobile">Phone</label>
                            <input className="form-control"
                                id="mobile"
                                type="text"
                                name="phone"
                                onChange={handleChange}
                                placeholder="Please Enter Mobile" />
                        </div>
                        <div className="form-group col-sm-12">
                            <label for="address">Address</label>
                            <textarea className="form-control"
                                 name="address" onChange={handleChange}>
                            </textarea>
                        </div>
                        <div className="form-group col-sm-6">
                            <label for="pass">Password</label>
                            <input className="form-control"
                                id="pass"
                                type="password"
                                name="password"
                                onChange={handleChange}
                                placeholder="Please Enter Password" />
                        </div>
                        <div className="form-group col-sm-6">
                            <label for="re-pass">Re-Enter Password</label>
                            <input className="form-control"
                                id="re-pass"
                                type="password"
                                name="re_password"
                                onChange={handleChange}
                                placeholder="Please Re-Enter Password" />
                        </div>
                    </div>
                    <div className="my-3 col-sm-12 row justify-content-center">
                        <button type="submit" value="Submit" className="btn btn-outline-primary">Register</button>
                    </div>
                </form>
            </div>
        </>
    )
}